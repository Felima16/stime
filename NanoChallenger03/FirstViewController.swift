//
//  TableViewController.swift
//  NanoChallenger03
//
//  Created by Vinicius Orlandi de Lima on 13/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import UIKit
import CoreData

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    // ARMAZENA OS NOMES DAS ATIVIDADES
    var names = [NSManagedObject]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let context = getContext()
        
        let fetchRequest: NSFetchRequest<Activity> = Activity.fetchRequest()
        
        do {
            //go get the results
            let results = try context.fetch(fetchRequest)
            
            names = results
            
        } catch {
            print("Error with request: \(error)")
        }
        
        self.navigationController?.isNavigationBarHidden = false
        self.tableView.delegate = self
        
        title = "Atividades"
        
        // registrando customCell
        let nib = UINib(nibName: "customCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "tblCell")

        
    }

    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let headerTitle = "Atividades"
//        return headerTitle
//    }

    /* BOTAO PARA CRIAR CELULAS
     bar button para adicionar atividades, onde salva as atividades no core*/
    
    @IBAction func addName(_ sender: AnyObject) {
        
        let alert = UIAlertController(title: "New Name",
                                      message: "Add a new name",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default,
                                       handler: { (action:UIAlertAction) -> Void in
                                        
                                        let textField = alert.textFields!.first
                                        self.saveName(name: textField!.text!)
                                        self.tableView.reloadData()
                                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
                                        self.navigationController?.show(vc, sender: nil)

        })
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default) { (action: UIAlertAction) -> Void in
        }
        
        alert.addTextField {
            (textField: UITextField) -> Void in
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert,
                              animated: true,
                              completion: {
        
        
        })
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    func saveName(name: String) {
        //1
        let context = getContext()
        
        //retrieve the entity that we just created
        let entityActivity =  NSEntityDescription.entity(forEntityName: "Activity", in: context)
        let newActivity = NSManagedObject(entity: entityActivity!, insertInto: context)
        let entityTime =  NSEntityDescription.entity(forEntityName: "Time", in: context)
        let newTime = NSManagedObject(entity: entityTime!, insertInto: context)
        let entityWorkDays =  NSEntityDescription.entity(forEntityName: "WorkDays", in: context)
        let newWorkDays = NSManagedObject(entity: entityWorkDays!, insertInto: context)
        
        
        //set the entity values
        newActivity.setValue(name, forKey: "name")
        
        //criando relacionamento um-pra-um
        newActivity.setValue(newTime, forKey: "time")
        
        //criando relacionamento um pra muito
        let work = newActivity.mutableSetValue(forKey: "workDays")
        work.add(newWorkDays)
        
        let fetchRequest: NSFetchRequest<Activity> = Activity.fetchRequest()
        
        //save the object
        do {
            try context.save()
            let results = try context.fetch(fetchRequest)
            self.names = results
            print("SALVO NO CORE!")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    // --------------------- CRIANDO TABELA ---------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt
        indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CustomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "tblCell") as! CustomTableViewCell
        
        let name = names[indexPath.section]
        
        cell.nameCell.text = name.value(forKey: "name") as? String
        
        // borda da celula
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.cornerRadius = 20
        cell.clipsToBounds = true
        
        // sombra na celula
        //        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        //        cell.layer.shadowColor = UIColor .black.cgColor
        //        cell.layer.shadowRadius = 10
        //        cell.layer.shadowOpacity = 0.5
        //        cell.layer.masksToBounds = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor .clear
        return headerView
    }
    
    
    
    //func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //    return "User"
    //}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
        self.navigationController?.show(vc, sender: nil)
        
    }
    
    
    

}

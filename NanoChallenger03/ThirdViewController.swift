//
//  ThirdViewController.swift
//  NanoChallenger03
//
//  Created by Pedro Victor Somensi on 9/15/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    @IBOutlet weak var normalView: UIView!
    @IBOutlet weak var turnoView: UIView!
    
    
    var viewShow : String = ""
    
    @IBInspectable var typeViewShow: String = "" {
        didSet {
            self.viewShow = typeViewShow
        }
    }
    
    //MARK : normalView
    @IBOutlet weak var dateEntrada: UIDatePicker!
    @IBOutlet weak var dateSaida: UIDatePicker!
    @IBOutlet weak var dateHoraExtras: UIDatePicker!
    @IBOutlet weak var dateEntradaFDS: UIDatePicker!
    @IBOutlet weak var dateSaidaFDS: UIDatePicker!
    @IBOutlet weak var dateHoraExtrasFDS: UIDatePicker!
    @IBOutlet weak var segundaButton: UIButton!
    @IBOutlet weak var tercaButton: UIButton!
    @IBOutlet weak var quartaButton: UIButton!
    @IBOutlet weak var quintaButton: UIButton!
    @IBOutlet weak var sextaButton: UIButton!
    @IBOutlet weak var sabadoButton: UIButton!
    @IBOutlet weak var domingoButton: UIButton!
    //MARK : END
    
    
    
    //MARK : TURNOVIEW
    @IBOutlet weak var dateHorasTrabalhadas: UIDatePicker!
    @IBOutlet weak var dateFolga: UIDatePicker!
    @IBOutlet weak var dateAlmoco: UIDatePicker!
    @IBOutlet weak var dateHoraExtrasT: UIDatePicker!
    
    
    
    
    // MARK : END
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.navigationController?.isNavigationBarHidden = false
        
        self.turnoView.isHidden = true
        self.normalView.isHidden = true
        
        self.verifyIdentityView()
        self.changeColorTextOfDatePicker()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
        self.turnoView.isHidden = true
        self.normalView.isHidden = true
        
        self.verifyIdentityView()
    }
    
    
    func verifyIdentityView(){
        if self.viewShow == "normalView" {
            self.normalView.isHidden = false
            self.turnoView.isHidden = true
        } else if self.viewShow == "turnoView" {
            self.normalView.isHidden = true
            self.turnoView.isHidden = false
        }
    }

    
    func changeColorTextOfDatePicker(){
        self.dateEntrada.setValue(UIColor.white, forKey: "textColor")
        self.dateSaida.setValue(UIColor.white, forKey: "textColor")
        self.dateHoraExtras.setValue(UIColor.white, forKey: "textColor")
        self.dateEntradaFDS.setValue(UIColor.white, forKey: "textColor")
        self.dateSaidaFDS.setValue(UIColor.white, forKey: "textColor")
        self.dateHoraExtrasFDS.setValue(UIColor.white, forKey: "textColor")
        
        self.dateHorasTrabalhadas.setValue(UIColor.white, forKey: "textColor")
        self.dateFolga.setValue(UIColor.white, forKey: "textColor")
        self.dateAlmoco.setValue(UIColor.white, forKey: "textColor")
        self.dateHoraExtrasT.setValue(UIColor.white, forKey: "textColor")
    }
    
    func borderButtons(){
        self.segundaButton.layer.borderWidth = 1
        self.segundaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.tercaButton.layer.borderWidth = 1
        self.tercaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.quartaButton.layer.borderWidth = 1
        self.quartaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.quintaButton.layer.borderWidth = 1
        self.quintaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.sextaButton.layer.borderWidth = 1
        self.sextaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.sabadoButton.layer.borderWidth = 1
        self.sabadoButton.layer.borderColor = UIColor.lightGray.cgColor
        self.domingoButton.layer.borderWidth = 1
        self.domingoButton.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    
    

}

//
//  CustomTableViewCell.swift
//  NanoChallenger03
//
//  Created by Vinicius Orlandi de Lima on 14/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var nameCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

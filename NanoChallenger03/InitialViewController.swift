//
//  InitialViewController.swift
//  NanoChallenger03
//
//  Created by Pedro Victor Somensi on 9/13/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import UIKit
import CoreData

class InitialViewController: UIViewController {

    @IBOutlet weak var textName: UITextField!
    
    @IBAction func enterButton(_ sender: AnyObject) {
        if self.textName.text != "" {
            
            self.name = self.textName.text!
            
           // salvando no CoreData
            
            let context = getContext()
            
            //retrieve the entity that we just created
            let entity =  NSEntityDescription.entity(forEntityName: "Person", in: context)
            
            let transc = NSManagedObject(entity: entity!, insertInto: context)
            
            //set the entity values
            transc.setValue(name, forKey: "name")
            
            //save the object
            do {
                try context.save()
                print("SALVO NO CORE!")
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            } catch {
                
            }
            // buscar dados "TESTE
            
            //create a fetch request, telling it about the entity
            let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
            
            do {
                //go get the results
                let searchResults = try getContext().fetch(fetchRequest)
                
                //I like to check the size of the returned results!
                print ("Quantidade de dados armazenados = \(searchResults.count)")
                
                //You need to convert to NSManagedObject to use 'for' loops
                for trans in searchResults as [NSManagedObject] {
                    //get the Key Value pairs (although there may be a better way to do that...
                    print("\(trans.value(forKey: "name"))")
                }
            } catch {
                print("Error with request: \(error)")
            }


            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
            self.present(vc, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Oops!", message:"Insira o nome no campo solicitado", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            self.present(alert, animated: true){}
        }
        
    }
    
    
    // guarda o nome do usuário
    var name : String = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // obter contexto
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
}

//
//  SecondViewController.swift
//  NanoChallenger03
//
//  Created by Pedro Victor Somensi on 9/14/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import UIKit
import CoreData

class SecondViewController: UIViewController{

    
    
    var atividade = [NSManagedObject]()
    @IBOutlet weak var textAtividade: UITextField!
    @IBOutlet weak var segmentedButton: UISegmentedControl!
    
    
    // MARK : Turno View
    @IBOutlet weak var turnoView: UIView!
    @IBOutlet weak var dateHorasTrabalhadas: UIDatePicker!
    @IBOutlet weak var dateFolga: UIDatePicker!
    @IBOutlet weak var dateAlmocoTurno: UIDatePicker!
    @IBOutlet weak var dateHorasExtras: UIDatePicker!
    // MARK : End
    
    
    //MARK : Normal View
    @IBOutlet weak var normalView: UIView!
    @IBOutlet weak var containerButtons: UIView!
    @IBOutlet weak var segundaButton: UIButton!
    @IBOutlet weak var tercaButton: UIButton!
    @IBOutlet weak var quartaButton: UIButton!
    @IBOutlet weak var quintaButton: UIButton!
    @IBOutlet weak var sextaButton: UIButton!
    
    
    @IBAction func tappedSegunda(_ sender: AnyObject) {
        self.clickButtonsWeek(indice: 0, button: self.segundaButton)
    }
    
    @IBAction func tappedTerca(_ sender: AnyObject) {
        self.clickButtonsWeek(indice: 1, button: self.tercaButton)
    }
    
    @IBAction func tappedQuarta(_ sender: AnyObject) {
        self.clickButtonsWeek(indice: 2, button: self.quartaButton)
    }
    
    @IBAction func tappedQuinta(_ sender: AnyObject) {
        self.clickButtonsWeek(indice: 3, button: self.quintaButton)
    }
    
    @IBAction func tappedSexta(_ sender: AnyObject) {
        self.clickButtonsWeek(indice: 4, button: self.sextaButton)
    }
    
    @IBAction func tappedSabado(_ sender: AnyObject) {
        self.clickButtonsWeek(indice: 5, button: self.sabadoButton)
    }
    
    @IBAction func tappedDomingo(_ sender: AnyObject) {
        self.clickButtonsWeek(indice: 6, button: self.domingoButton)
    }
    
    
    
    @IBOutlet weak var dateEntrada: UIDatePicker!
    @IBOutlet weak var dateSaida: UIDatePicker!
    @IBOutlet weak var dateAlmoco: UIDatePicker!
    
    @IBOutlet weak var dateEntradaFDS: UIDatePicker!
    @IBOutlet weak var dateSaidaFDS: UIDatePicker!
    @IBOutlet weak var dateAlmocoFDS: UIDatePicker!
    
    
    @IBOutlet weak var containerButtonsFooter: UIView!
        @IBOutlet weak var sabadoButton: UIButton!
        @IBOutlet weak var domingoButton: UIButton!
    
    
    
    var buttonsBool : [Bool] = [false, false, false, false, false, false, false]
    
    
    //MARK : End
    
    
    
    @IBAction func tappedSegmentedButton(_ sender: AnyObject) {
        
        switch self.segmentedButton.selectedSegmentIndex {
        case 0:
            UIView.animate(withDuration: 0.3, animations: {
                self.turnoView.isHidden = false
                self.normalView.isHidden = true
                
            })
            
            UIView.animate(withDuration: 0.4, animations: {
                self.normalView.isHidden = true
                }, completion: { (true) in
                    UIView.animate(withDuration: 0.3, animations: {
                        self.turnoView.isHidden = false
                    })
            })
        case 1:
            UIView.animate(withDuration: 0.3, animations: {
                self.turnoView.isHidden = true
                self.normalView.isHidden = false
            })
        default:
            break
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = false
        self.normalView.isHidden = true
        
        self.borderButtons()
        //self.changeColorTextOfDatePicker()
        
        // -------------------------- BOTAO VOLTAR DA NAVIGATION ----------------------------------------
        
//        let btn = UIButton()
//        btn.addTarget(self, action: #selector(SecondViewController "CHAMA FUNCAO PARA SALVAR NO CORE"), for: .touchUpInside)
//        let item = UIBarButtonItem()
//        item.customView = btn
//        
//        self.navigationItem.leftBarButtonItem = item
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    
    func borderButtons(){
        self.segundaButton.layer.borderWidth = 1
        self.segundaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.tercaButton.layer.borderWidth = 1
        self.tercaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.quartaButton.layer.borderWidth = 1
        self.quartaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.quintaButton.layer.borderWidth = 1
        self.quintaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.sextaButton.layer.borderWidth = 1
        self.sextaButton.layer.borderColor = UIColor.lightGray.cgColor
        self.sabadoButton.layer.borderWidth = 1
        self.sabadoButton.layer.borderColor = UIColor.lightGray.cgColor
        self.domingoButton.layer.borderWidth = 1
        self.domingoButton.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func changeColorTextOfDatePicker(){
        //turno change colors
        self.dateHorasTrabalhadas.setValue(UIColor.white, forKey: "textColor")
        self.dateFolga.setValue(UIColor.white, forKey: "textColor")
        self.dateAlmocoTurno.setValue(UIColor.white, forKey: "textColor")
        self.dateHorasExtras.setValue(UIColor.white, forKey: "textColor")
        
        //normal change colors
        self.dateEntrada.setValue(UIColor.white, forKey: "textColor")
        self.dateSaida.setValue(UIColor.white, forKey: "textColor")
        self.dateAlmoco.setValue(UIColor.white, forKey: "textColor")
        //fds
        self.dateEntradaFDS.setValue(UIColor.white, forKey: "textColor")
        self.dateSaidaFDS.setValue(UIColor.white, forKey: "textColor")
        self.dateAlmocoFDS.setValue(UIColor.white, forKey: "textColor")
        
    }
    
    func clickButtonsWeek(indice : Int!, button : UIButton!){
        if button.backgroundColor != UIColor.lightGray {
            button.backgroundColor = UIColor.lightGray
            self.buttonsBool[indice] = true
        } else {
            button.backgroundColor = UIColor.clear
            self.buttonsBool[indice] = false
        }
    }
    
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func saveNormal(entradaUtil: Date, saidaUtil: Date, almocoUtil: Date, entradaFds: Date, saidaFds: Date, almocoFds: Date, normal: String){
        
        let context = getContext()
        
        //instanciando entidades
        let entityTime = NSEntityDescription.entity(forEntityName: "Time", in: context)
        let newTime = NSManagedObject(entity: entityTime!, insertInto: context)
        let entityWeak = NSEntityDescription.entity(forEntityName: "Weak", in: context)
        let newWeak = NSManagedObject(entity: entityWeak!, insertInto: context)
        
        //salvando atributos
        newTime.setValue(entradaUtil, forKey: "entradaUtil")
        newTime.setValue(saidaUtil, forKey: "saidaUtil")
        newTime.setValue(almocoUtil, forKey: "almocoUtil")
        newTime.setValue(entradaFds, forKey: "entradaFds")
        newTime.setValue(saidaFds, forKey: "saidaFds")
        newTime.setValue(almocoFds, forKey: "almocoFds")
        newTime.setValue(normal, forKey: "normal")
        
        //criando relacionamento
        newTime.setValue(newWeak, forKey: "weak")
        
        //request
        let fetchRequest: NSFetchRequest<Time> = Time.fetchRequest()
        
        //salvando objetos
        do {
            try context.save()
            let results = try context.fetch(fetchRequest)
            self.atividade = results
            print("SALVO NO CORE!")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func saveTurno(entrada: Date, saida: Date, hrExtra: Date, hrsTrabalhadas: Date, turno: String){
        
        let context = getContext()
        
        //instanciando entidades
        let entityTime = NSEntityDescription.entity(forEntityName: "Time", in: context)
        let newTime = NSManagedObject(entity: entityTime!, insertInto: context)
        let entityWeak = NSEntityDescription.entity(forEntityName: "Weak", in: context)
        let newWeak = NSManagedObject(entity: entityWeak!, insertInto: context)
        
        
        //salvando atributos
        newTime.setValue(entrada, forKey: "entrada")
        newTime.setValue(saida, forKey: "saida")
        newTime.setValue(hrExtra, forKey: "extra")
        newTime.setValue(hrsTrabalhadas, forKey: "hrsTrabalhadas")
        newTime.setValue(turno, forKey: "turno")
        
        //criando relacionamento
        newTime.setValue(newWeak, forKey: "weak")
        
        //request
        let fetchRequest: NSFetchRequest<Time> = Time.fetchRequest()
        
        //salvando objetos
        do {
            try context.save()
            let results = try context.fetch(fetchRequest)
            self.atividade = results
            print("SALVO NO CORE!")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
  
}

//
//  Weak+CoreDataProperties.swift
//  NanoChallenger03
//
//  Created by Vinicius Orlandi de Lima on 16/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import Foundation
import CoreData

extension Weak {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weak> {
        return NSFetchRequest<Weak>(entityName: "Weak");
    }

    @NSManaged public var domingo: Bool
    @NSManaged public var sabado: Bool
    @NSManaged public var uteis: Bool
    @NSManaged public var time: Time?

}

//
//  graphView.swift
//
//
//  Created by Pedro Victor Somensi on 03/06/16.
//
//

import UIKit

let π:CGFloat = CGFloat(M_PI)

@IBDesignable class graphView: UIView {
    
    var circle : Bool = true
    let sections = 3
    
    @IBInspectable var horasAtraso: CGFloat = 0.0 {
        didSet {
            alternatives[0] = horasAtraso
        }
    }
    @IBInspectable var horasExtras: CGFloat  = 0.0 {
        didSet {
            alternatives[1] = horasExtras
        }
    }
    
    @IBInspectable var horasAlmoco : CGFloat = 0.0 {
        didSet {
            alternatives[2] = horasAlmoco
        }
    }
    
    @IBInspectable var horasTrabalhadas: CGFloat  = 0.0
    @IBInspectable var alternatives: [CGFloat] = [0, 0, 0]
    let percentuals: [CGFloat] = [0, 0, 0]
    
    
    let colors : [UIColor] = [UIColor(red: 51/255, green: 53/255, blue: 83/255, alpha: 1), UIColor.init(red: 234/255.0, green: 212/255.0, blue: 28/255.0, alpha: 1), UIColor.init(red: 34/255.0, green: 21/255.0, blue: 28/255.0, alpha: 1)]
    
    override func draw(_ rect: CGRect){
        //location in view
        let radius = min(self.bounds.width, self.bounds.height) / 4
        
        let graphCenter = CGPoint(x:radius * 2, y: bounds.height/2)
        
        let arcWidth = min(bounds.width, bounds.height) / 2
        var startAngle = 0 * π
        var lastPercent : CGFloat = 0
        var endAngle =  (2 * π)
        
        for i in 0..<sections {
            let percent = alternatives[i] / horasTrabalhadas
            endAngle = (2 * π) * (lastPercent + percent)
            let auxPath = UIBezierPath(arcCenter: graphCenter,
                                       radius: radius,
                                       startAngle: startAngle,
                                       endAngle: endAngle,
                                       clockwise: true)
            
                        let medianAngle = (endAngle + startAngle) / 2
                        let auxPoint = CGPoint(x: graphCenter.x + radius * cos(medianAngle), y: graphCenter.y + radius * sin(medianAngle))
            
                        if percent > 0.05 {
                            let label = UILabel()
                            label.font = UIFont(name: "Verdana", size: arcWidth / CGFloat(sections + 1))
                            label.textColor = UIColor.white
                            label.text = String(format: "%.2f", percent * 100) + "%"
                            label.sizeToFit()
                            label.textAlignment = .center
                            label.center = auxPoint
            
                            self.addSubview(label)
                        }
            startAngle = endAngle
            lastPercent += percent
            auxPath.lineWidth = arcWidth
            colors[i].setStroke()
            auxPath.stroke()
        }
    }
    
   

}

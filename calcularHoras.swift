//
//  calcularHoras.swift
//  NanoChallenger03.
//
//  Created by Fernanda  de Lima on 13/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import UIKit
import UserNotifications

class calcularHoras {
    
    //variaveis para armazenar as horas
    
    //bool
    var notificar: Bool = true
    var trabalhando: Bool = false
    
    //para o dia
    private var dataHoje: NSDate!
    private var horaChegou: NSDate!
    private var horaSaiu: NSDate!
    private var horaAlmoco: NSDate!
    private var horaAlmocoVolta: NSDate!
    
    
    //get from core data
    var horaSaida: NSDate!
    var horaComeca: NSDate!
    var horaTrabalhar: NSDate!
    var horaExtraLimite: NSDate!
    
    // set from core data
    var tempoAtraso: NSDate!
    var tempoTrabalhado: NSDate!
    var tempoExtraDiurno: NSDate!
    var tempoExtraNoturno: NSDate!
    var tempoExtra: NSDate!
    
    
//    init() {
//        horaChegou = toNSDate(h: 9, m: 15)
//        horaTrabalhar = toNSDate(h: 6, m: 0)
//        horaSaiu = toNSDate(h: 16, m: 0)
//        
//        print(horaChegou)
//        print(horaTrabalhar)
//        print(horaSaiu)
//        
//    }
    
    //MARK: - calculos
    
    //funçao para calcular o tempo de atraso
    func calAtraso() -> String {
        
        let hora = horaChegou.hour() - horaComeca.hour()
        var min = horaChegou.minute() - horaComeca.minute()
        
        if min < 0 {
            min = min * -1
        }
        
        //verifica se o minuto é menor que 10 e coloca o zero na frente para padrao hh:mm
        if min < 10{
            return "\(hora):0\(min)"
        }else{
            return "\(hora):\(min)"
        }
    }
    
    
    //funçao para calcular a hora extra
    func calExtraDiurna() -> String {
        
        //variaveis auxiliares
        var horaC = 0
        var minC = 0
        var horaS = 0
        var minS = 0
        
        //verifica se tem hora extra pela manha e marca o tempo
        if ExtraChegada(){
            horaC = horaComeca.hour() - horaChegou.hour()
            minC = horaComeca.minute() - horaChegou.minute()
            
            if minC < 0 {
                minC = minC * -1
            }
        }
        
        //verifica se tem hora extra na saida e marca o tempo
        if ExtraSaida(){
            horaS = horaSaiu.hour() - horaSaida.hour()
            minS = horaSaiu.minute() - horaSaida.minute()
            if minS < 0 {
                minS = minS * -1
            }
        }
        
        //faz as soma das horas extras
        var hora = horaS + horaC
        var min = minS + minC
        
        //verifica se os minutos sao maiores que 60 e soma uma hora
        if min >= 60{
            min = min - 60
            hora = hora + 1
        }
        
        //verifica se o minuto é menor que 10 e coloca o zero na frente para padrao hh:mm
        if min < 10{
            return "\(hora):0\(min)"
        }else{
            return "\(hora):\(min)"
        }
    }
    
    
    //funçao para calcular a hora extra noturna
    func calExtraNoturno() -> String {
        
        var hora = 0
        let min = horaSaiu.minute()
        
        if horaSaiu.hour() < 24 && horaSaiu.hour() >= 22 {
            hora = horaSaiu.hour() - 22
        }else{
            hora = horaSaiu.hour() + 2
        }
        
        //verifica se o minuto é menor que 10 e coloca o zero na frente para padrao hh:mm
        if min < 10{
            return "\(hora):0\(min)"
        }else{
            return "\(hora):\(min)"
        }
    }
    
    //calcula hora trabalhada para horarios flexiveis
    func calHoraTrabalhada() -> String {
        
        var horaSaiuMin = 0
        
        //Verifica se passou das 24hr
        if horaSaiu.hour() - horaChegou.hour() < 0 {
            horaSaiuMin = (24 - horaChegou.hour() + horaSaiu.hour())*60 + horaSaiu.minute()
            
        }else{
            horaSaiuMin = horaSaiu.hour() * 60 + horaSaiu.minute()
            
        }
        
        //transforma em minutos para comparaçao
        let horaCheMin = horaChegou.hour() * 60 + horaChegou.minute()
        var hora = 0
        var min = 0
        
        let horaTra = horaSaiuMin - horaCheMin
        
        //normaliza os minutos para horas
        if horaTra > 60 {
            hora = horaTra/60
            min = hora%60
        }else{
            min = horaTra
        }
        
        //verifica se o minuto é menor que 10 e coloca o zero na frente para padrao hh:mm
        if min < 10{
            return "\(hora):0\(min)"
        }else{
            return "\(hora):\(min)"
        }
    }
    
    //funçao para calcular a hora extra flexivel
    func calExtra() -> String {
        
        var horaSaiuMin = 0
        
        //Verifica se passou das 24hr
        if horaSaiu.hour() - horaChegou.hour() < 0 {
            horaSaiuMin = (24 - horaChegou.hour() + horaSaiu.hour())*60 + horaSaiu.minute()
            
        }else{
            horaSaiuMin = horaSaiu.hour() * 60 + horaSaiu.minute()
            
        }
        
        let horaTraMin = horaTrabalhar.hour() * 60 + horaTrabalhar.minute()
        var hora = 0
        var min = 0
        
        let extra = horaSaiuMin - horaTraMin
        
        
        //normalizando os minutos
        if  extra > 60 {
            hora = extra/60
            min = hora%60
        }else{
            min = extra
        }
        
        //verifica se o minuto é menor que 10 e coloca o zero na frente para padrao hh:mm
        if min < 10{
            return "\(hora):0\(min)"
        }else{
            return "\(hora):\(min)"
        }
    }
    
    
    //MARK: - gets
    //hora chegou
    func getHoraChegou() -> NSDate {
        return horaChegou
    }
    
    //data do dia
    func getDataHoje() -> NSDate {
        return dataHoje
    }
    
    // hora saiu
    func getHoraSaiu() -> NSDate {
        return horaSaiu
    }
    
    //hora Almoco
    func gethoraAlmoco() -> NSDate {
        return horaAlmoco
    }
    
    //hora Almoco Volta
    func gethoraAlmocoVolta() -> NSDate {
        return horaAlmocoVolta
    }
    
    
    //MARK: - sets
    
    //hora de chegada
    func setHoraChegou(chegou:NSDate) {
        horaChegou = chegou
    }
    
    //hora do almoço
    func setHoraAlmoco(almoco:NSDate) {
        horaAlmoco = almoco
    }
    
    //hora do fim do almoço
    func setHoraAlmocoVolta(almocoAcabou:NSDate) {
        horaAlmocoVolta = almocoAcabou
    }
    
    //hora de ir embora
    func setHoraSaiu(saiu:NSDate) {
        horaSaiu = saiu
    }
    
    
    //MARK: - Bool
    
    //Verificar se esta atrasado
    func AtrasadoChegada() -> Bool {
        if horaChegou.isGreaterThanDate(dateToCompare:horaComeca.addMinutes(minToAdd: 5)) {
            return true
        }
        return false
    }
    
    //verificar se chegou mais cedo - hora extra
    func ExtraChegada() -> Bool {
        if horaChegou.isLessThanDate(dateToCompare:horaComeca.addMinutes(minToAdd: -5)) {
            return true
        }
        return false
    }
    
    //verificar se passou da hora - hora extra
    func ExtraSaida() -> Bool {
        if horaSaida.addMinutes(minToAdd: 5).isLessThanDate(dateToCompare:horaSaiu) {
            return true
        }
        return false
    }
    
    //verificar se é norturno- hora extra
    func ExtraNoturno() -> Bool {
        
        if (horaSaiu.hour() >= 22 && horaSaiu.minute() >= 05) || horaSaiu.hour() < 5{
            return true
        }
        return false
    }
    
    //verifica se voltou antes do horario de almoço
    func ExtraAlmoco() -> Bool{
        let horaVolta = horaAlmoco.addMinutes(minToAdd: 60)
        
        if horaAlmocoVolta.isLessThanDate(dateToCompare: horaVolta){
            return true
        }
        
        return false
    }
    
    //verificar se trabalhou no horario certo
    func Extra() -> Bool {
        var horaSaiuMin = 0
        
        //Verifica se passou das 24hr
        if horaSaiu.hour() - horaChegou.hour() < 0 {
            horaSaiuMin = (24 - horaChegou.hour() + horaSaiu.hour())*60 + horaSaiu.minute()
            
        }else{
            horaSaiuMin = horaSaiu.hour() * 60 + horaSaiu.minute()
            
        }
        
        let horaTraMin = horaTrabalhar.hour() * 60 + horaTrabalhar.minute()
        let horaCheMin = horaChegou.hour()*60 + horaChegou.minute()
        
        if horaSaiuMin - horaCheMin > horaTraMin{
            return true
        }
        
        return false
    }
    
    
    //MARK: - Funções
    
    //notificação para a volta do almoço
    func notiVoltaAlmoco(){
        
        //cria a hora que falta para o almoço terminar menos 15 min
        let faltaAlmoco = horaAlmoco.addMinutes(minToAdd: 45)
        let aviso = NSDateComponents()
        aviso.hour = faltaAlmoco.hour()
        aviso.minute = faltaAlmoco.minute()
        
        //Cria o corpo da notificaçao
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: "STime", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "15 min restantes de almoço", arguments: nil)
        content.sound = UNNotificationSound.default()
        
        // Deliver the notification
        let trigger = UNCalendarNotificationTrigger.init(dateMatching: aviso as DateComponents, repeats: false)
        let request = UNNotificationRequest.init(identifier: "Rest15Almoco", content: content, trigger: trigger)
        
        // Schedule the notification.
        let center = UNUserNotificationCenter.current()
        center.add(request)
        
    }
    
    //notificação para o limite das horas extras
    func notiFimHoraExtra(){
        
        //verificar a hora para marcar a notificaçao para uma possivel hora extra
        var min = horaChegou.minute() + horaTrabalhar.minute() + horaExtraLimite.minute() - 15
        var hora = 0
        
        //normalizando os minutos
        if  min > 60 {
            hora = min/60
            min = hora%60
        }
        
        let aviso = NSDateComponents()
        aviso.hour = horaChegou.hour() + horaTrabalhar.hour() + horaExtraLimite.hour() + hora
        aviso.minute = min
        
        //Cria o corpo da notificaçao
        let content1 = UNMutableNotificationContent()
        content1.title = NSString.localizedUserNotificationString(forKey: "STime", arguments: nil)
        content1.body = NSString.localizedUserNotificationString(forKey: "15 min restantes de hora extra", arguments: nil)
        content1.sound = UNNotificationSound.default()
        
        let content2 = UNMutableNotificationContent()
        content2.title = NSString.localizedUserNotificationString(forKey: "STime", arguments: nil)
        content2.body = NSString.localizedUserNotificationString(forKey: "10 min restantes de hora extra", arguments: nil)
        content2.sound = UNNotificationSound.default()
        
        let content3 = UNMutableNotificationContent()
        content3.title = NSString.localizedUserNotificationString(forKey: "STime", arguments: nil)
        content3.body = NSString.localizedUserNotificationString(forKey: "5 min restantes de hora extra", arguments: nil)
        content3.sound = UNNotificationSound.default()
        
        // Deliver the notification
        let trigger1 = UNCalendarNotificationTrigger.init(dateMatching: aviso as DateComponents, repeats: false)
        let request1 = UNNotificationRequest.init(identifier: "Rest15Extra", content: content1, trigger: trigger1)
        
        if aviso.minute + 5 >= 60{
            aviso.minute = (5 + aviso.minute) - 60
            aviso.hour += 1
        }else{
            aviso.minute += 5
        }
        
        let trigger2 = UNCalendarNotificationTrigger.init(dateMatching: aviso as DateComponents, repeats: false)
        let request2 = UNNotificationRequest.init(identifier: "Rest10Extra", content: content2, trigger: trigger2)
        
        if aviso.minute + 5 >= 60{
            aviso.minute = (5 + aviso.minute) - 60
            aviso.hour += 1
        }else{
            aviso.minute += 5
        }
        
        let trigger3 = UNCalendarNotificationTrigger.init(dateMatching: aviso as DateComponents, repeats: false)
        let request3 = UNNotificationRequest.init(identifier: "Rest5Extra", content: content3, trigger: trigger3)
        
        // Schedule the notification.
        let center = UNUserNotificationCenter.current()
        center.add(request1)
        center.add(request2)
        center.add(request3)
    }
    
    //salvar info no core
    func salveCore(){
        
    }
    
    func toNSDate(h:Int,m:Int) -> NSDate {
        
        //        let dateComponents = NSDateComponents()
        ////        dateComponents.year = NSDate().
        ////        dateComponents.month = 7
        ////        dateComponents.day = 11
        //        dateComponents.hour = h
        //        dateComponents.minute = m
        //
        //        // Create date from components
        //        let userCalendar = NSCalendar.current // user calendar
        //        let someDateTime = userCalendar.date(from: dateComponents as DateComponents)
        
        let data = NSDate()
        
        data.addMinutes(minToAdd: -data.minute())
        data.addHour(hourToAdd: -data.hour())
        
        print(data)
        
        data.addMinutes(minToAdd:m)
        data.addHour(hourToAdd: h)
        
        print(data)
        return data
    }
    
    
    //reset
    func reset(){
        trabalhando = false
        
        //para o dia
        dataHoje = NSDate()
        horaChegou = NSDate()
        horaSaiu = NSDate()
        horaAlmoco = NSDate()
        horaAlmocoVolta = NSDate()
        
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
    }
    
    
    
}

extension NSDate
{
    func hour() -> Int
    {
        //Get Hour
        let calendar = NSCalendar.current
        let components = calendar.component(.hour, from: self as Date)
        
        return components
    }
    
    
    func minute() -> Int
    {
        //Get Minute
        let calendar = NSCalendar.current
        let components = calendar.component(.minute, from: self as Date)
        
        return components
    }
    
    func toShortTimeString() -> String
    {
        //Get Short Time String
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        let timeString = formatter.string(from: self as Date)
        
        //Return Short Time String
        return timeString
    }
    
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    
    func isSameThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isSame = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isSame = true
        }
        
        //Return Result
        return isSame
    }
    
    func addMinutes(minToAdd: Int) -> NSDate {
        let secondsInMinutes: TimeInterval = Double(minToAdd) * 60
        let dateWithMinAdded: NSDate = self.addingTimeInterval(secondsInMinutes)
        
        //Return Result
        return dateWithMinAdded
    }
    
    func addHour(hourToAdd: Int) -> NSDate {
        let secondsInHour: TimeInterval = Double(hourToAdd) * 60 * 60
        let dateWithHourAdded: NSDate = self.addingTimeInterval(secondsInHour)
        
        //Return Result
        return dateWithHourAdded
    }
    
}

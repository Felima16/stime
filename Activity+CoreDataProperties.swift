//
//  Activity+CoreDataProperties.swift
//  NanoChallenger03
//
//  Created by Vinicius Orlandi de Lima on 14/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import Foundation
import CoreData


extension Activity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Activity> {
        return NSFetchRequest<Activity>(entityName: "Activity");
    }

    @NSManaged public var name: String?
    
    @NSManaged public var time: Time?
    @NSManaged public var workDays: WorkDays?

}

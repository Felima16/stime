//
//  Person+CoreDataProperties.swift
//  NanoChallenger03
//
//  Created by Vinicius Orlandi de Lima on 13/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import UIKit
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person");
    }

    @NSManaged public var name: String?

}

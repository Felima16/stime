//
//  WorkDays+CoreDataProperties.swift
//  NanoChallenger03
//
//  Created by Vinicius Orlandi de Lima on 16/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import Foundation
import CoreData

extension WorkDays {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WorkDays> {
        return NSFetchRequest<WorkDays>(entityName: "WorkDays");
    }

    @NSManaged public var entrada: NSDate?
    @NSManaged public var extra: NSDate?
    @NSManaged public var extraDiurno: NSDate?
    @NSManaged public var extraNoturno: NSDate?
    @NSManaged public var hrsTrabalhas: NSDate?
    @NSManaged public var saida: NSDate?
    @NSManaged public var atrasado : NSDate?
    @NSManaged public var activity: Activity?

}

//
//  Time+CoreDataProperties.swift
//  NanoChallenger03
//
//  Created by Vinicius Orlandi de Lima on 16/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import Foundation
import CoreData

extension Time {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Time> {
        return NSFetchRequest<Time>(entityName: "Time");
    }
    
    @NSManaged public var extra: NSDate?
    @NSManaged public var trabalho: NSDate?
    
    @NSManaged public var entradaUtil: NSDate?
    @NSManaged public var saidaUtil: NSDate?
    @NSManaged public var almocoUtil: NSDate?
    
    @NSManaged public var entradaFds: NSDate?
    @NSManaged public var saidaFds: NSDate?
    @NSManaged public var almocoFds: NSDate?
    
    @NSManaged public var turno: String?
    @NSManaged public var normal: String?
    
    @NSManaged public var activity: Activity?
    @NSManaged public var weak: Weak?

}

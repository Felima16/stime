//
//  InterfaceController.swift
//  Watch Extension
//
//  Created by Pedro Victor Somensi on 9/15/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    @IBOutlet var buttonEntrada: WKInterfaceButton!
    @IBOutlet var buttonAlmoco: WKInterfaceButton!
    @IBOutlet var buttonSaida: WKInterfaceButton!
    @IBOutlet var buttonVoltaAlmoco: WKInterfaceButton!

    
    var calc = calcularHoras()
    
    //botao saida
    @IBAction func actionSaida() {
        buttonSaida.setHidden(true)
        buttonEntrada.setHidden(false)
    }
    
    //botao volta almoço
    @IBAction func actionVoltaAlmoco() {
        buttonVoltaAlmoco.setHidden(true)
        
        buttonAlmoco.setBackgroundColor(UIColor.clear)
        buttonAlmoco.setEnabled(false)
        buttonAlmoco.setHidden(false)
    }
    
    //botao entrada
    @IBAction func actionEntrada() {
        buttonSaida.setHidden(false)
        buttonEntrada.setHidden(true)
        
        buttonAlmoco.setBackgroundColor(UIColor(red: 25/255, green: 150/255, blue: 170/255, alpha: 1))
        buttonAlmoco.setEnabled(true)
        
        calc.setHoraChegou(chegou: NSDate())
        
        if calc.AtrasadoChegada(){
            //Mostra o tempo de atraso
            
            //chegada.text = "Atrasado: \(calc.calAtraso())"
        }else{
            //mostra a hora que chegou
            //            presentAlert(withTitle: "Chegada", message: NSDate().toShortTimeString(), preferredStyle: .alert, actions: <#T##[WKAlertAction]#>)
        }
    }
    
    //botao almoco
    @IBAction func actionAlmoco() {
        buttonAlmoco.setHidden(true)
        buttonVoltaAlmoco.setHidden(false)
    }

    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}

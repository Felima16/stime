//
//  secondDAO.swift
//  NanoChallenger03
//
//  Created by Vinicius Orlandi de Lima on 16/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import UIKit
import CoreData

class secondDAO: NSObject {
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    

}

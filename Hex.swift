//
//  Hex.swift
//  NanoChallenger03
//
//  Created by Vinicius Orlandi de Lima on 14/09/16.
//  Copyright © 2016 PedroSomensi. All rights reserved.
//

import UIKit

class Hex: NSObject {
    
    func hexColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines as NSCharacterSet) as CharacterSet)
        
        if (cString.hasPrefix("#")) {
     
            cString = cString.substring(to: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

}
